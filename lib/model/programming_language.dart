class ProgrammingLanguage {
  String name;
  String tipe;
  String description;
  int year;
  String inventor;
  String imageAsset;
  List<String> imageUrls;

  ProgrammingLanguage({
    required this.name,
    required this.tipe,
    required this.description,
    required this.year,
    required this.inventor,
    required this.imageAsset,
    required this.imageUrls,
  });
}

var programmingLanguageList = [
  ProgrammingLanguage(
    name: 'Java',
    tipe: 'Bahasa Pemrograman Berorientasi Objek',
    description:
        'Java adalah bahasa pemrograman berorientasi objek yang dirancang untuk portabilitas dan kinerja tinggi. Memiliki sintaksis yang jelas dan berorientasi pada objek, Java banyak digunakan untuk pengembangan perangkat lunak enterprise dan aplikasi mobile.',
    year: 1995,
    inventor: 'James Gosling',
    imageAsset: 'assets/images/Logo-Java.png',
    imageUrls: [
      'https://pbs.twimg.com/media/ELhG9tYU0AAYmRY.png',
      'https://logolook.net/wp-content/uploads/2022/11/Java-Logo-history.png'
    ],
  ),
  ProgrammingLanguage(
    name: 'Haskell',
    tipe: 'Bahasa Pemrograman Fungsional',
    description:
        'Haskell adalah bahasa pemrograman fungsional yang menekankan pemrograman deklaratif dan penggunaan fungsi sebagai nilai pertama. Haskell membantu dalam pengembangan perangkat lunak yang aman, bersih, dan mudah diuji.',
    year: 1998,
    inventor: 'Philip Wadler, Simon Peyton Jones',
    imageAsset: 'assets/images/Haskell-Logo.png',
    imageUrls: [
      'https://upload.wikimedia.org/wikipedia/en/thumb/4/4d/Logo_of_the_Haskell_programming_language.svg/2560px-Logo_of_the_Haskell_programming_language.svg.png',
      'https://wiki.haskell.org/wikiupload/8/89/Haskell_logo_ideas_6_falconnl.png'
    ],
  ),
  ProgrammingLanguage(
    name: 'C',
    tipe: 'Bahasa Pemrograman Prosedural',
    description:
        'C adalah bahasa pemrograman prosedural yang fokus pada pemrograman struktural dan efisiensi. Digunakan untuk pengembangan sistem operasi, perangkat lunak embedded, dan perangkat keras, C memiliki kontrol langsung terhadap memori dan sumber daya sistem.',
    year: 1970,
    inventor: 'Dennis Ritchie',
    imageAsset: 'assets/images/Logo-C.jpg',
    imageUrls: [
      'https://academy.alterra.id/blog/wp-content/uploads/2021/07/Logo-C.png',
      'https://student-activity.binus.ac.id/himmat/wp-content/uploads/sites/14/2021/09/Banner-Ricky.jpg'
    ],
  ),
  ProgrammingLanguage(
    name: 'Python',
    tipe: 'Bahasa Pemrograman Berbasis Skrip',
    description:
        'Python adalah bahasa pemrograman berbasis skrip yang bersifat serbaguna, mudah dibaca, dan dapat diintegrasikan dengan mudah. Python umumnya digunakan untuk pengembangan web, analisis data, dan kecerdasan buatan.',
    year: 1991,
    inventor: 'Guido van Rossum',
    imageAsset: 'assets/images/Logo-Python.png',
    imageUrls: [
      'https://crocodic.com/wp-content/uploads/2023/06/Thumbnail-Blog-3.png',
      'https://teknojurnal.com/wp-content/uploads/2015/08/PYTHON-HEADER.jpg'
    ],
  ),
  ProgrammingLanguage(
    name: 'Scala',
    tipe: 'Bahasa Pemrograman Berbasis Objek dan Fungsional',
    description:
        'Scala adalah bahasa pemrograman yang menggabungkan paradigma berorientasi objek dan fungsional. Dirancang untuk ekspresivitas dan kelincahan pengembangan, Scala sering digunakan untuk pengembangan aplikasi besar dan kompleks.',
    year: 2003,
    inventor: 'Martin Odersky',
    imageAsset: 'assets/images/Scala-Logo.png',
    imageUrls: [
      'https://miro.medium.com/v2/resize:fit:1200/1*teF2BfJWP1n5b3Js-Y_TZA.png',
      'https://googiehost.com/blog/wp-content/uploads/2023/02/SCALA-Programming-Languages-for-AI-Development.jpg'
    ],
  ),
  ProgrammingLanguage(
    name: 'Dart',
    tipe: 'Bahasa Pemrograman Lintas Platform',
    description:
        'Dart dirancang dan dikembangkan oleh Lars Bak dan Kasper Lund di Google yang secara resmi diperkenalkan pada Oktober 2011 di Konferensi GOTO, Denmark dan versi pertama Dart yaitu versi 1.0 resmi dirilis pada Nov 2013.',
    year: 2013,
    inventor: 'Lars Bak dan Kasper Lund',
    imageAsset: 'assets/images/Logo-Dart.png',
    imageUrls: [
      'https://www.gamelab.id/uploads/news/berita-681-mengenal-lebih-jauh-tentang-dart-20210417-093153.jpg',
      'https://w7.pngwing.com/pngs/564/348/png-transparent-dart-flutter-google-software-development-kit-past-and-future-text-logo-web-application.png'
    ],
  ),
];